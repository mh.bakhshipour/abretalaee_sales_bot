class ActionMapper:
    class ActionKeyNotFound(Exception):
        pass

    actions = {
        'start_survey': 'شروع تست',
        'about_us': 'درباره ما',
        'contact_us': 'تماس با ما',
    }

    @classmethod
    def get(cls, key):
        action = cls.actions.get(key, None)
        if action is None:
            raise cls.ActionKeyNotFound('Action `{}` not registered'.format(key))

        return action
