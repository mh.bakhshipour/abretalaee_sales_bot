from survey.domains import StaticMessage, TelegramUser, Question
from survey.repositories import StaticMessageRepository, TelegramUserRepository, QuestionRepository
from survey.use_cases import StartUseCase, InitSurveyUseCase, GetPhoneNumberUseCase, AnswerQuestion, \
    GetEmailAddressUseCase
from survey.utils import ActionMapper


def get_phone_number_use_case():
    return GetPhoneNumberUseCase(
        static_message_repository=StaticMessageRepository(model=StaticMessage),
        telegram_user_repository=TelegramUserRepository(model=TelegramUser),
        action_mapper=ActionMapper,
    )


def get_email_use_case():
    return GetEmailAddressUseCase(
        static_message_repository=StaticMessageRepository(model=StaticMessage),
        telegram_user_repository=TelegramUserRepository(model=TelegramUser),
        action_mapper=ActionMapper,
    )


def start_use_case():
    return StartUseCase(
        static_message_repository=StaticMessageRepository(model=StaticMessage),
        telegram_user_repository=TelegramUserRepository(model=TelegramUser),
        action_mapper=ActionMapper,
    )


def init_survey_use_case():
    return InitSurveyUseCase(
        static_message_repository=StaticMessageRepository(model=StaticMessage),
        telegram_user_repository=TelegramUserRepository(model=TelegramUser),
        question_repository=QuestionRepository(model=Question),
        action_mapper=ActionMapper,
    )


def answer_query_use_case():
    return AnswerQuestion(
        static_message_repository=StaticMessageRepository(model=StaticMessage),
        telegram_user_repository=TelegramUserRepository(model=TelegramUser),
        question_repository=QuestionRepository(model=Question),
        action_mapper=ActionMapper,
    )
