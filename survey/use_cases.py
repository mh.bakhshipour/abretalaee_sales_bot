import re

from survey.domains import Answer
from survey.requests import StartResponseObject, InitSurveyResponse, GetPhoneNumberResponse, AnswerQuestionResponse


class BaseUseCase:
    def __init__(self, *args, **kwargs):
        self.sr = kwargs.get('static_message_repository')
        self.tr = kwargs.get('telegram_user_repository')
        self.am = kwargs.get('action_mapper')


class StartUseCase(BaseUseCase):

    def execute(self, start_request):
        u = self.tr.get_or_create(**start_request.fields)
        text_response = str(self.sr.find_message_by_key('start').value).format(
            first_name=u.first_name,
            last_name=u.last_name
        )
        give_phone_number_text = self.sr.find_message_by_key('phone_number_request').value
        r = StartResponseObject({
            'text_response': [text_response, give_phone_number_text],
        })
        return r


class InitSurveyUseCase(BaseUseCase):

    def __init__(self, *args, **kwargs):
        self.qr = kwargs.get('question_repository')
        super().__init__(*args, **kwargs)

    def execute(self, init_survey_request):
        user = self.tr.find_user_by_telegram_id(init_survey_request.telegram_id)
        survey_step = len(user.answer)
        total_steps = self.qr.get_total_questions_count()
        response_payload = {}
        if survey_step == 0:
            response_payload['text_response'] = self.sr.find_message_by_key('init_survey').value
            response_payload['questions'] = [self.qr.get_question_by_order(survey_step + 1).to_mongo().to_dict()]

        if survey_step == total_steps:
            user.delete()
            response_payload['text_response'] = self.sr.find_message_by_key('init_survey').value
            response_payload['questions'] = [self.qr.get_question_by_order(survey_step + 1).to_mongo().to_dict()]

        if 0 < survey_step < total_steps:
            response_payload['questions'] = [self.qr.get_question_by_order(survey_step + 1).to_mongo().to_dict()]

        return InitSurveyResponse(response_payload)


class GetPhoneNumberUseCase(BaseUseCase):

    def execute(self, get_phone_number_request):
        self.tr.update_phone_number(get_phone_number_request.telegram_id, get_phone_number_request.phone_number)
        return GetPhoneNumberResponse({
            'text_response': [self.sr.find_message_by_key('phone_number_success').value]
        })


class GetEmailAddressUseCase(BaseUseCase):

    def execute(self, get_phone_number_request):
        self.tr.update_email_address(get_phone_number_request.telegram_id, get_phone_number_request.email)
        return GetPhoneNumberResponse({
            'text_response': [self.sr.find_message_by_key('email_success').value],
            'action_options': [
                [self.am.get('start_survey')],
                [self.am.get('about_us'), self.am.get('contact_us')]
            ]
        })


class AnswerQuestion(BaseUseCase):
    class AlreadyAnsweredException(Exception):
        pass

    def __init__(self, *args, **kwargs):
        self.qr = kwargs.get('question_repository')
        super().__init__(*args, **kwargs)

    def find_appropriate_score_message(self, score):
        if int(round((100 * score) / 35)) >= 95:
            return self.sr.find_message_by_key("MORE_THAN_95").value
        elif 65 < int(round((100 * score) / 35)) < 95:
            return self.sr.find_message_by_key("BETWEEN_65_95").value
        elif int(round((100 * score) / 35)) < 65:
            return self.sr.find_message_by_key("BELOW_65").value

    def execute(self, request):
        if request.callback_id == 'ALREADY_ANSWERED':
            raise self.AlreadyAnsweredException

        split_callback_id = str(request.callback_id).split('_')
        user = self.tr.find_user_by_telegram_id(request.telegram_id)
        question_id = split_callback_id[0]
        option_id = split_callback_id[1]
        question = self.qr.find_question_by_id(question_id)
        option = self.qr.find_option_by_id(option_id)
        self.tr.add_to_answers(
            user,
            Answer(
                question_title=question.title,
                answer_title=option.title,
                question_id=question.id,
                answer_id=option._id,
                score=option.score
            )
        )
        user.total_score = user.total_score + option.score
        user.save()

        response_payload = {}
        survey_step = len(user.answer)
        step_count = self.qr.get_total_questions_count()
        if step_count == survey_step:
            response_payload['survey_finished_text'] = str(self.sr.find_message_by_key('survey_finished').value).format(
                round((100 * user.total_score) / 35))
            response_payload['score_text'] = self.find_appropriate_score_message(user.total_score)
            response_payload['selected_option_text'] = option.title
        else:
            response_payload['selected_option_text'] = option.title
            response_payload['questions'] = [self.qr.get_question_by_order(survey_step + 1).to_mongo().to_dict()]

        return AnswerQuestionResponse(response_payload)
