from bson import ObjectId
from mongoengine import *


class Option(EmbeddedDocument):
    _id = ObjectIdField(required=True, default=lambda: ObjectId())
    title = StringField(required=True)
    score = IntField()


class Question(Document):
    title = StringField(required=True)
    order = IntField(required=True)
    options = EmbeddedDocumentListField(Option)
    meta = {'collection': 'questions'}


class Answer(EmbeddedDocument):
    question_title = StringField(required=True)
    answer_title = StringField(required=True)
    question_id = ObjectIdField(required=True)
    answer_id = ObjectIdField(required=True)
    score = IntField()


class StaticMessage(Document):
    key = StringField(required=True)
    value = StringField(required=True)
    meta = {'collection': 'static_messages'}


class TelegramUser(Document):
    telegram_id = IntField(required=True)
    user_type = StringField(required=True)
    first_name = StringField()
    last_name = StringField()
    total_score = IntField()
    email = EmailField()
    phone_number = StringField(default=None)
    answer = ListField(EmbeddedDocumentField(Answer))

    meta = {'collection': 'telegram_users'}
