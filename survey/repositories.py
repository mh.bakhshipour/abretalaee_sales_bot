from survey.domains import StaticMessage, TelegramUser, Answer


class BaseRepository:
    def __init__(self, *args, **kwargs):
        model = kwargs.get('model', None)
        if model is None:
            raise ValueError('Parameter `model` should be instance of a `Document`')
        self.model = model


class StaticMessageRepository(BaseRepository):

    def find_message_by_key(self, key):
        return self.model.objects.get(key=key)


class TelegramUserRepository(BaseRepository):

    def find_user_by_telegram_id(self, telegram_id):
        return self.model.objects.get(telegram_id=telegram_id)

    @staticmethod
    def add_to_answers(user, answer):
        user.answer.append(answer)
        user.save()

    def update_phone_number(self, telegram_id, phone_number):
        return self.model.objects(telegram_id=telegram_id).update(**{'phone_number': phone_number})

    def update_email_address(self, telegram_id, email):
        return self.model.objects(telegram_id=telegram_id).update(**{'email': email})

    def get_or_create(self,first_name, last_name, user_type, telegram_id, total_score):
        # Not safe as not using transactions, and no default transaction for mongodb
        try:
            return self.model.objects.get(telegram_id=telegram_id)
        except TelegramUser.DoesNotExist:
            return self.model(
                first_name=first_name,
                last_name=last_name, user_type=user_type,
                phone_number=None,
                total_score=total_score,
                telegram_id=telegram_id
            ).save()


class QuestionRepository(BaseRepository):

    def get_question_by_order(self, order):
        return self.model.objects.get(order=order)

    def find_question_by_id(self, question_id):
        return self.model.objects.get(id=question_id)

    def find_option_by_id(self, option_id):
        options = self.model.objects.filter(options__match={"_id": option_id})[0].options
        for o in options:
            if str(o._id) == str(option_id):
                return o
        else:
            print("kireeekhaaar")

    def get_total_questions_count(self):
        return self.model.objects.count()
