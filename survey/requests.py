import collections


class InvalidRequestObject(object):

    def __init__(self):
        self.errors = []

    def add_error(self, parameter, message):
        self.errors.append({'parameter': parameter, 'message': message})

    def has_errors(self):
        return len(self.errors) > 0

    @staticmethod
    def __nonzero__():
        return False

    __bool__ = __nonzero__


class ValidRequestResponseObject(object):

    def __init__(self, fields=None):
        self.fields = fields

    @property
    def telegram_id(self):
        return self.fields.get('telegram_id', None)

    @property
    def action_options(self):
        return self.fields.get('action_options', None)

    @property
    def text_response(self):
        return self.fields.get('text_response', None)

    @classmethod
    def from_dict(cls, adict):
        return cls(fields=adict)

    @staticmethod
    def __nonzero__():
        return True

    __bool__ = __nonzero__


class StartRequestObject(ValidRequestResponseObject):
    pass


class StartResponseObject(ValidRequestResponseObject):
    pass


class InitSurveyRequest(ValidRequestResponseObject):
    pass


class InitSurveyResponse(ValidRequestResponseObject):

    @property
    def questions(self):
        return self.fields.get('questions', None)


class GetPhoneNumberRequest(ValidRequestResponseObject):

    @property
    def phone_number(self):
        return self.fields['phone_number']


class GetPhoneNumberResponse(ValidRequestResponseObject):
    def has_error(self):
        return 'error' in self.fields


class AnswerQuestionRequest(ValidRequestResponseObject):
    @property
    def callback_id(self):
        return self.fields['callback_id']


class AnswerQuestionResponse(ValidRequestResponseObject):
    @property
    def questions(self):
        return self.fields.get('questions', None)

    @property
    def selected_option_text(self):
        return self.fields.get('selected_option_text', None)

    @property
    def survey_finished_text(self):
        return self.fields.get('survey_finished_text', None)

    @property
    def score_text(self):
        return self.fields.get('score_text', None)


class GetEmailRequest(ValidRequestResponseObject):

    @property
    def email(self):
        return self.fields['email']