import logging
import os

from telegram.ext import Updater

from bot import fill_dispatcher
from utils import load_env_variables, connect_to_database

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)
logger = logging.getLogger(__name__)

load_env_variables()
connect_to_database()

if __name__ == '__main__':
    REQUEST_KWARGS = {'proxy_url': os.getenv('PROXY', None)}
    updater = Updater(token=os.getenv('TELEGRAM_BOT_TOKEN'), use_context=True, workers=int(os.getenv('WORKERS')),
                      request_kwargs=REQUEST_KWARGS)
    dispatcher = updater.dispatcher
    fill_dispatcher(dispatcher)
    updater.start_polling()
    updater.idle()
