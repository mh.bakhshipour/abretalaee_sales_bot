import logging
from telegram import Bot
import os
import sys

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


def check_input():
    if len(sys.argv) < 2:
        print('Error: Please provide a web_hook_url to be set in telegram servers')
        exit()


def set_web_hook():
    web_hook_url = sys.argv[1]
    bot = Bot(token=os.getenv('TELEGRAM_BOT_TOKEN'))
    response = bot.set_webhook(web_hook_url)
    print(str(response))
    print('Done')


if __name__ == '__main__':
    check_input()
    set_web_hook()
