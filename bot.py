from telegram import ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup

from survey.builders import start_use_case, init_survey_use_case, get_phone_number_use_case, answer_query_use_case, \
    get_email_use_case
from survey.domains import StaticMessage
from survey.repositories import StaticMessageRepository
from survey.requests import StartRequestObject, InitSurveyRequest, GetPhoneNumberRequest, AnswerQuestionRequest, \
    GetEmailRequest

from telegram.ext import CommandHandler, MessageHandler, Filters, ConversationHandler, CallbackQueryHandler

from survey.use_cases import AnswerQuestion
from survey.utils import ActionMapper


def create_question_layout(response, update):
    questions = response.questions
    if len(questions):
        for q in questions:
            inline_keyboard = [[]]
            for option in q["options"]:
                inline_keyboard[0].append(
                    InlineKeyboardButton(option["title"], callback_data=str(q['_id']) + '_' + str(option['_id'])))
            inline_keyboard = InlineKeyboardMarkup(inline_keyboard)
            if update.message:
                update.message.reply_text(q["title"], reply_markup=inline_keyboard)
            else:
                update.callback_query.message.reply_text(q["title"], reply_markup=inline_keyboard)


def start(update, _):
    request = StartRequestObject.from_dict({
        'telegram_id': update.message.chat.id,
        'user_type': update.message.chat.type,
        'first_name': update.message.chat.first_name,
        'total_score': 0,
        'last_name': update.message.chat.last_name
    })
    use_case = start_use_case()
    response = use_case.execute(request)
    for message in response.text_response:
        update.message.reply_text(message)


def get_cellphone_number(update, _):
    request = GetPhoneNumberRequest.from_dict({
        'telegram_id': update.message.chat.id,
        'phone_number': update.message.text,
    })
    use_case = get_phone_number_use_case()
    response = use_case.execute(request)
    for message in response.text_response:
        update.message.reply_text(text=message)


def init_survey(update, _):
    request = InitSurveyRequest.from_dict({'telegram_id': update.message.chat.id})
    use_case = init_survey_use_case()
    response = use_case.execute(request)
    if response.text_response:
        update.message.reply_text(response.text_response)
    if response.questions:
        create_question_layout(response, update)


def update_question(response, query):
    keyboard = [
        [InlineKeyboardButton("پاسخ شما: {}".format(response.selected_option_text),
                              callback_data="ALREADY_ANSWERED")]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_text(
        text=query.message.text,
        reply_markup=reply_markup
    )


def answer_question(update, _):
    query = update.callback_query

    request = AnswerQuestionRequest.from_dict({
        'telegram_id': update.callback_query.message.chat.id,
        'callback_id': query.data
    })

    use_case = answer_query_use_case()
    try:
        response = use_case.execute(request)
    except AnswerQuestion.AlreadyAnsweredException:
        query.answer("شما قبلا پاسخ این سوال را داده اید.")
        return

    if response.questions:
        query.answer()
        update_question(response, query)
        create_question_layout(response, update)

    elif response.survey_finished_text:
        query.answer()
        update_question(response, query)
        update.callback_query.message.reply_text(response.survey_finished_text)
        update.callback_query.message.reply_text(response.score_text)


def about_us(update, _):
    r = StaticMessageRepository(model=StaticMessage)
    about_us_message = r.find_message_by_key("about_us").value
    update.message.reply_text(about_us_message)


def contact_us(update, _):
    r = StaticMessageRepository(model=StaticMessage)
    contact_us_message = r.find_message_by_key("contact_us").value
    update.message.reply_text(contact_us_message)


def email_address(update, _):
    request = GetEmailRequest.from_dict({
        'telegram_id': update.message.chat.id,
        'email': update.message.text,
    })
    use_case = get_email_use_case()
    response = use_case.execute(request)
    for message in response.text_response:
        update.message.reply_text(
            text=message,
            reply_markup=ReplyKeyboardMarkup(response.action_options, one_time_keyboard=True)
        )


def fill_dispatcher(dispatcher):
    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(
        MessageHandler(Filters.regex('^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'), email_address))
    dispatcher.add_handler(MessageHandler(Filters.regex('^09[0-9]{9}$'), get_cellphone_number))
    dispatcher.add_handler(MessageHandler(Filters.regex('^' + ActionMapper.get('start_survey') + '$'), init_survey))
    dispatcher.add_handler(CallbackQueryHandler(answer_question))
    dispatcher.add_handler(MessageHandler(Filters.regex('^' + ActionMapper.get('contact_us') + '$'), contact_us))
    dispatcher.add_handler(MessageHandler(Filters.regex('^' + ActionMapper.get('about_us') + '$'), about_us))

    return dispatcher
