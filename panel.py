from flask import Flask
from flask_admin import Admin
from flask_admin.contrib.mongoengine import ModelView
from flask_basicauth import BasicAuth
from flask_mongoengine import MongoEngine
from survey.domains import TelegramUser
from utils import load_env_variables
import os

load_env_variables()

db = MongoEngine()
app = Flask(__name__)

app.config['MONGODB_SETTINGS'] = {
    'db': os.getenv('DATABASE_NAME'),
    'host': os.getenv('DATABASE_HOST'),
    'port': int(os.getenv('DATABASE_PORT'))
}

db.init_app(app)

app.config['BASIC_AUTH_USERNAME'] = '09389627001'
app.config['BASIC_AUTH_PASSWORD'] = 'F5cfsAsjeVdMWYbA'

basic_auth = BasicAuth(app)
app.config['BASIC_AUTH_FORCE'] = True

app.config['FLASK_ADMIN_SWATCH'] = 'cerulean'
admin = Admin(app, name='Abre Talaee', template_mode='bootstrap3')


class TelegramUserAdmin(ModelView):
    form_subdocuments = {
        'answer': {
            'form_subdocuments': {
                None: {
                    'form_columns': ('question_title', 'answer_title', 'question_id', 'answer_id', 'score')
                }
            }
        }
    }


admin.add_view(TelegramUserAdmin(TelegramUser))

# Add administrative views here

app.run(host="0.0.0.0", port=5000)
